package ru.itpark.gardeningnotes.service;

import ru.itpark.gardeningnotes.entity.Steed;

import java.util.List;

public interface SteedService {

    void add(Steed steed);
    List<Steed> findAll();
    void deleteById(int id);
    List<Steed> findAllByArea(double totalArea);
}
