package ru.itpark.gardeningnotes.service;

import ru.itpark.gardeningnotes.entity.Bed;

import java.util.List;

public interface BedService {
    void add(Bed bed);
    List<Bed> findAll();
    List<Bed> findAllBySteedId(int steedId);
    void deleteById (int steedId, int id);
    List<Bed> findAllByCulture(String culture);
}
