package ru.itpark.gardeningnotes.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itpark.gardeningnotes.entity.Bed;
import ru.itpark.gardeningnotes.repository.BedRepository;
import ru.itpark.gardeningnotes.repository.SteedRepository;

import java.util.List;

@Service
public class BedServiceImpl implements BedService {
    private final BedRepository bedRepository;
    private final SteedRepository steedRepository;

    @Autowired
    public BedServiceImpl(BedRepository bedRepository, SteedRepository steedRepository) {
        this.bedRepository = bedRepository;
        this.steedRepository = steedRepository;
    }

    @Override
    public void add(Bed bed) {
        bedRepository.save(bed);
    }

    @Override
    public List<Bed> findAll() {
        return bedRepository.findAll();
    }

    @Override
    public List<Bed> findAllBySteedId(int steedId) {
        return bedRepository.findAllBySteedId(steedId);
    }


    @Override
    public void deleteById(int steedId, int id) {
        bedRepository.deleteBySteedIdAndId(steedId, id);

    }

    @Override
    public List<Bed> findAllByCulture(String culture) {
        return bedRepository.findAllByCultureContainingIgnoreCase(culture);
    }
}
