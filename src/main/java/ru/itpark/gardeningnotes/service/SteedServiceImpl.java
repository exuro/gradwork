package ru.itpark.gardeningnotes.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itpark.gardeningnotes.entity.Steed;
import ru.itpark.gardeningnotes.repository.BedRepository;
import ru.itpark.gardeningnotes.repository.SteedRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class SteedServiceImpl implements  SteedService {
    private final SteedRepository steedRepository;
    private final BedRepository bedRepository;

    @Autowired
    public SteedServiceImpl(SteedRepository steedRepository, BedRepository bedRepository) {
        this.steedRepository = steedRepository;
        this.bedRepository = bedRepository;
    }

    @Override
    public void add(Steed steed) {
        steedRepository.save(steed);
    }

    @Override
    public List<Steed> findAll() {
        return steedRepository.findAll();
    }

    @Transactional
    @Override
    public void deleteById(int id) {
        bedRepository.deleteBySteedId(id);
        steedRepository.delete(id);
    }

    @Override
    public List<Steed> findAllByArea(double totalArea) {
        return steedRepository.findAllByTotalArea(totalArea);
    }
}
