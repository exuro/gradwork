package ru.itpark.gardeningnotes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.itpark.gardeningnotes.entity.Steed;
import ru.itpark.gardeningnotes.service.SteedService;

import java.util.List;

@RestController
@RequestMapping("/steeds")
public class SteedController {
    private final SteedService steedService;

    @Autowired
    public SteedController(SteedService steedService) {
        this.steedService = steedService;
    }

    @GetMapping
    public List<Steed> getAll(){
        return steedService.findAll();
    }

    @GetMapping(path = "/search", params = "totalArea")
    public List<Steed> searchByArea(@RequestParam double totalArea){
        return steedService.findAllByArea(totalArea);
    }

    @PostMapping
    public void add(@RequestBody Steed steed){
        steedService.add(steed);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id){
        steedService.deleteById(id);
    }


}
