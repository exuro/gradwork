package ru.itpark.gardeningnotes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.itpark.gardeningnotes.entity.Bed;
import ru.itpark.gardeningnotes.service.BedService;

import java.util.List;

@RestController
@RequestMapping("/steeds/{steedId}/beds")
public class BedController {
    private final BedService bedService;

    @Autowired
    public BedController(BedService bedService) {
        this.bedService = bedService;
    }

    @GetMapping
    public List<Bed> getAll(){
        return bedService.findAll();
    }

    @GetMapping(path = "/search", params = "steedId")
    public List<Bed> getAllBySteedId(@PathVariable int steedId){
        return bedService.findAllBySteedId(steedId);
    }

    @GetMapping(path = "/find", params = "culture")
    public List<Bed> getAllByCulture(String culture){
        return bedService.findAllByCulture(culture);
    }

    @PostMapping
    public void add(@PathVariable int steedId, @RequestBody Bed bed){
        bed.setSteedId(steedId);
        bedService.add(bed);
    }

    @DeleteMapping("/{id}")
    public void delete (@PathVariable int steedId, @PathVariable int id){
        bedService.deleteById(steedId, id);
    }


}
