package ru.itpark.gardeningnotes.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Steed {
    @Id
    @GeneratedValue
    private int id;
    private double totalArea;

    public Steed() {
    }

    public Steed(double totalArea) {
        this.totalArea = totalArea;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getTotalArea() {
        return totalArea;
    }

    public void setTotalArea(double totalArea) {
        this.totalArea = totalArea;
    }
}
