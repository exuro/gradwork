package ru.itpark.gardeningnotes.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Bed {

    @Id
    @GeneratedValue
    private int id;
    private int steedId;
    private  String culture;
    private double area;

    public Bed() {
    }

    public Bed(int steedId, String culture, double area) {
        this.steedId = steedId;
        this.culture = culture;
        this.area = area;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSteedId() {
        return steedId;
    }

    public void setSteedId(int steedId) {
        this.steedId = steedId;
    }

    public String getCulture() {
        return culture;
    }

    public void setCulture(String culture) {
        this.culture = culture;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }
}
