package ru.itpark.gardeningnotes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itpark.gardeningnotes.entity.Bed;

import java.util.List;

public interface BedRepository extends JpaRepository<Bed, Integer> {
    List<Bed> findAllBySteedId(int steedId);
    void deleteBySteedIdAndId(int steedId, int id);
    List<Bed> findAllByCultureContainingIgnoreCase(String culture);
    void deleteBySteedId(int id);
}
