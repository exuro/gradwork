package ru.itpark.gardeningnotes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itpark.gardeningnotes.entity.Steed;

import java.util.List;

@Repository
public interface SteedRepository extends JpaRepository<Steed, Integer> {
    List<Steed> findAllByTotalArea(double totalArea);
}
